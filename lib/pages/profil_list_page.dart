import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_login_demo/pages/basvuran_detail.dart';
import 'package:flutter_login_demo/pages/basvurandetail_page.dart';
import 'package:flutter_login_demo/pages/companyDetail.dart';
import 'package:flutter_login_demo/test_detail/test_detail.dart';

class ProfilListPage extends StatefulWidget {
  @override
  _ProfilListPageState createState() => _ProfilListPageState();
}

class _ProfilListPageState extends State<ProfilListPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView(
        children: <Widget>[
          homeWidget("Bahadır KALAY", "02.07.2019",
              "http://pttkep.gov.tr/uploads/haber-ve-duyurular/online-kayit-online-basvuru.jpg"),
          homeWidget("Eren BOZARIK", "03.07.2019",
              "http://pttkep.gov.tr/uploads/haber-ve-duyurular/online-kayit-online-basvuru.jpg"),
          homeWidget("Onur ERGÜLER", "03.07.2019",
              "http://pttkep.gov.tr/uploads/haber-ve-duyurular/online-kayit-online-basvuru.jpg")
        ],
      ),
    );
  }
}

Widget homeWidget(String title, String date, String images) {
  return InkWell(
    onTap: () {
      Navigator.of(context)
            .push(MaterialPageRoute(builder: (BuildContext context) {
          return CompanyDetail();
        }));
    },
    child: Container(
      margin: EdgeInsets.all(10.0),
      width: 250.0,
      height: 375.0,
      child: Stack(
        children: <Widget>[
          Container(
            width: 300.0,
            height: 400.0,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(20.0),
                  bottomLeft: Radius.circular(20.0),
                  bottomRight: Radius.circular(20.0)),
              image: DecorationImage(
                colorFilter: ColorFilter.mode(Colors.amber, BlendMode.overlay),
                image: NetworkImage('$images'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Positioned(
            bottom: 1,
            right: 5,
            child: FloatingActionButton(
              heroTag: title,
              mini: true,
              backgroundColor: Color.fromRGBO(37, 38, 94, 1),
              child: Icon(
                Icons.chevron_right,
                color: Colors.white,
              ),
              onPressed: () {
                MaterialPageRoute(builder: (context) => TestDetail());
                //MaterialPageRoute(builder: (context) => TestDetail());
                //Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => TestDetail()));
                //Navigator.of(context).pushNamed("/detail");
              },
            ),
          ),
          Positioned(
            bottom: 30,
            left: 5,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(title,
                    style: TextStyle(
                      color: Color.fromRGBO(37, 38, 94, 1),
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0,
                    )),
                Row(
                  children: <Widget>[
                    Icon(Icons.date_range,
                        color: Color.fromRGBO(37, 38, 94, 1)),
                    Text(date,
                        style: TextStyle(
                            color: Color.fromRGBO(37, 38, 94, 1),
                            fontSize: 17.0)),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}
