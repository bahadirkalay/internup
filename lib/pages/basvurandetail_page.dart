import 'package:flutter/material.dart';
import 'package:flutter_login_demo/pages/basvuran_detail.dart';

class BasvuranDetailPage extends StatefulWidget {
  @override
  _BasvuranDetailPageState createState() => _BasvuranDetailPageState();
}

class _BasvuranDetailPageState extends State<BasvuranDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Başvuran Detail Page"),
        
      ),
      body: BasvuranDetail(),
    );
    
  }
}
