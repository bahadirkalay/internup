import 'package:flutter/material.dart';
import 'package:flutter_login_demo/test_detail/my_custom_form.dart';
class TestDetail extends StatefulWidget {
  @override
  _TestDetailState createState() => _TestDetailState();
}

class _TestDetailState extends State<TestDetail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        bottomOpacity: 0.0,
        backgroundColor: Color.fromRGBO(37, 38, 94, 1),
        title: Text("Test Detail Page"),
      ),
      body: MyCustomForm()
    );
  }
}