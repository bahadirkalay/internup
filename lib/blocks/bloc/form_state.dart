import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class FormState extends Equatable {
  FormState([List props = const []]) : super(props);
}

class InitialFormState extends FormState {}
